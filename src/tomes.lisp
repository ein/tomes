
(in-package #:tomes)


(defun check-bounds (x y console)
  (let ((width (cols console))
        (height (rows console)))
    (cond ((not (<= 0 x (1- width)))
           (cerror "Ignore" "X: ~d is out of bounds, should be below ~d" x width)
           nil)

          ((not (<= 0 y (1- height)))
           (cerror "Ignore" "Y: ~d is out of bounds, should be below ~d" y width)
           nil)
          (t t))))


(defun draw-glyph-id (glyph-id x y console)
  (when (check-bounds x y console)
    (with-slots (renderer tileset tileset-texture) console
      (with-slots (glyph-width glyph-height cols) tileset
        (multiple-value-bind (gy gx) (floor glyph-id cols)
          (with-sdl-rects ((src (* gx glyph-width) (* gy glyph-height) glyph-width glyph-height)
                           (dst (* x glyph-width) (* y glyph-height) glyph-width glyph-height))
            (check-sdl (sdl:sdl-render-copy renderer tileset-texture src dst)))))))
  (values))



(defun show (object x y &optional (console *default-console*))
  (typecase object
    (integer (draw-glyph-id (find-glyph-id object (encoding (tileset console))) x y console))
    (character (show (char-code object) x y console))
    (string (loop :for c :across object :for i :from x :do (show (char-code c) i y console)))
    (t (cerror "Ignore" "Don't know how to SHOW a ~a (at ~d/~d)"
               (type-of object) x y))))




(defun show-colored (object x y color &optional (console *default-console*))
  (let ((tex (tileset-texture console)))
    (cffi:with-foreign-objects ((r :uint8) (g :uint8) (b :uint8))
      (sdl:sdl-gettexture-color-mod tex r g b)
      (sdl:sdl-set-texture-color-mod tex
                                     (min 255 (max 0 (aref color 0)))
                                     (min 255 (max 0 (aref color 1)))
                                     (min 255 (max 0 (aref color 2))))
      (show object x y console)
      (sdl:sdl-set-texture-color-mod tex (cffi:mem-ref r :uint8) (cffi:mem-ref g :uint8) (cffi:mem-ref b :uint8))
      (values))))


(defmacro with-color ((console color) &body body)
  (a:with-gensyms (tex c r g b)
    `(let ((,tex (tileset-texture ,console))
           (,c ,color))
       (cffi:with-foreign-objects ((,r :uint8) (,g :uint8) (,b :uint8))
         (sdl:sdl-gettexture-color-mod ,tex ,r ,g ,b)
         (sdl:sdl-set-texture-color-mod ,tex
                                        (min 255 (max 0 (aref ,c 0)))
                                        (min 255 (max 0 (aref ,c 1)))
                                        (min 255 (max 0 (aref ,c 2))))
         ,@body
         (sdl:sdl-set-texture-color-mod ,tex (cffi:mem-ref ,r :uint8) (cffi:mem-ref ,g :uint8) (cffi:mem-ref ,b :uint8))))))



(defun foreground-color (&optional (console *default-console*))
  (cffi:with-foreign-objects ((r :uint8) (g :uint8) (b :uint8))
    (sdl:sdl-gettexture-color-mod (tileset-texture console) r g b)
    (make-array 3 :element-type '(unsigned-byte 8)
                  :initial-contents (list (cffi:mem-ref r :uint8) (cffi:mem-ref g :uint8) (cffi:mem-ref b :uint8)))))


(defun (setf foreground-color) (color &optional (console *default-console*))
  (sdl:sdl-set-texture-color-mod (tileset-texture console)
                                 (min 255 (max 0 (aref color 0)))
                                 (min 255 (max 0 (aref color 1)))
                                 (min 255 (max 0 (aref color 2))))
  color)

  
