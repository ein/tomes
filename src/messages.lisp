
(in-package #:tomes)



;; move this into the CONNECTION class once the interface is stabilised
(defparameter *messages* (make-hash-table :test 'eq))



(defun resolve-class (object)
  (if (typep object 'class)
      object
      (class-of object)))


(defun register-message (object &optional (console *default-console*))
  (let ((class (resolve-class object)))
    (setf (gethash class *messages*) nil)))



(defun ensure-registered (test class message)
  (unless test
    (cerror "Ignore" "The class ~w of object ~w has not been registered as a message"
            class message)))



(defun post-message (message &optional (on-error :warn) (console *default-console*))
  (let* ((class (class-of message))
         (msgs *messages*))
    (multiple-value-bind (handlers found?) (gethash class msgs)
      (ensure-registered found? class message)
      (map 'nil (lambda (handler) (funcall handler message)) handlers))))



(defun subscribe-to (message handler &optional (on-error :warn) (console *default-console*))
  (let* ((msgs *messages*)
         (class (resolve-class message)))
    (multiple-value-bind (handlers found?) (gethash class msgs)
      (declare (ignore handlers))
      (ensure-registered found? class message)
      (push handler (gethash class *messages*)))))


(defmacro subscribe ((name class) &body body)
  `(subscribe-to (find-class ',class) (lambda (,name) ,@body)))



(defgeneric send-to (object message))


(defclass key-up ()
  ((keysym :reader key-sym)))


(defclass key-down ()
  ((keysym :reader key-sym)))


(defclass button-up ()
  ((button :reader button)
   (x :reader mouse-x)
   (y :reader mouse-y)))


(defclass button-down ()
  ((button :reader button)
   (x :reader mouse-x)
   (y :reader mouse-y)))


(defclass window-resized ()
  ((new-width :reader new-width)
   (new-height :reader new-height)))




(defun post-system-messages (console)
  (cffi:with-foreign-object (event 'sdl:sdl-event)
    (flet ((keyslot (slot)
             (cffi:foreign-slot-value event 'sdl:sdl-keyboard-event slot))
           (buttonslot (slot)
             (cffi:foreign-slot-value event 'sdl:sdl-mouse-button-event slot))
           (symslot (sym slot)
             (cffi:foreign-slot-value sym 'sdl:sdl-keysym slot)))
      (loop :until (zerop (sdl:sdl-poll-event event)) :do
        (case (cffi:foreign-slot-value event 'sdl:sdl-event 'sdl:type)
          (#.sdl:+sdl-keydown+
           (let* ((msg (make-instance 'key-down)))
             (setf (slot-value msg 'keysym) (symslot (keyslot 'sdl:keysym) 'sdl:scancode))
             (post-message msg nil console)))
          (#.sdl:+sdl-keyup+
           (let* ((msg (make-instance 'key-up)))
             (setf (slot-value msg 'keysym) (symslot (keyslot 'sdl:keysym) 'sdl:scancode))
             (post-message msg nil console)))
          (#.sdl:+sdl-mousebuttondown+
           (let* ((msg (make-instance 'button-down)))
             (setf (slot-value msg 'button) (buttonslot 'sdl:button)
                   (slot-value msg 'x) (buttonslot 'sdl:x)
                   (slot-value msg 'y) (buttonslot 'sdl:y))
             (post-message msg nil console)))
          (#.sdl:+sdl-mousebuttonup+
           (let* ((msg (make-instance 'button-up)))
             (setf (slot-value msg 'button) (buttonslot 'sdl:button)
                   (slot-value msg 'x) (buttonslot 'sdl:x)
                   (slot-value msg 'y) (buttonslot 'sdl:y))
             (post-message msg nil console))))))))



;; Should be called from the make-connection function eventually, certainly not from user-code
(defun initialize-default-messages ()
  (register-message (find-class 'key-down))
  (register-message (find-class 'key-up))
  (register-message (find-class 'button-down))
  (register-message (find-class 'button-up)))



(defun clear-message (message)
  (let ((class (resolve-class message))
        (messages *messages*))
    (multiple-value-bind (handler found?) (gethash class messages)
      (declare (ignore handler))
      (ensure-registered found? class message)
      (setf (gethash class messages) nil))))
