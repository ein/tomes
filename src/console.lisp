
(in-package #:tomes)


(defvar *sdl-count* 0)


(defclass console ()
  ((cols :initarg :cols :reader cols)
   (rows :initarg :rows :reader rows)
   (tileset :initarg :tileset :accessor tileset)
   (tileset-texture :initarg :texture :accessor tileset-texture)
   (renderer :initarg :renderer :reader renderer)
   (window :initarg :window :reader window)
   (event :initarg :event :reader event)))



(defun open-console (title rows cols tileset)
  (let* ((width (* cols (glyph-width tileset)))
         (height (* rows (glyph-height tileset)))
         window renderer texture)

    (when (zerop *sdl-count*) (check-sdl (sdl:sdl-init (logior
                                                        sdl:+sdl-init-video+
                                                        sdl:+sdl-init-events+))))
    (incf *sdl-count*)

    (handler-bind ((error (lambda (e)
                            (declare (ignore e))
                            (when texture (sdl:sdl-destroy-texture texture))
                            (when renderer (sdl:sdl-destroy-renderer renderer))
                            (when window (sdl:sdl-destroy-window window)))))
      (setf window (check-sdl (sdl:sdl-create-window
                               title
                               sdl:+sdl-windowpos-undefined+ sdl:+sdl-windowpos-undefined+
                               width height
                               (logior sdl:+sdl-window-allow-highdpi+ sdl:+sdl-window-shown+))))

      (setf renderer (check-sdl (sdl:sdl-create-renderer
                                 window -1
                                 (logior sdl:+sdl-renderer-accelerated+
                                         sdl:+sdl-renderer-presentvsync+))))

      ;; load in the glyph texture
      (pngload:with-png-in-static-vector (png (path tileset))
        
        (setf texture (check-sdl (sdl:sdl-create-texture renderer
                                                         sdl:+sdl-pixelformat-argb8888+
                                                         sdl:+sdl-textureaccess-static+
                                                         (pngload:width png)
                                                         (pngload:height png))))

        (sdl:sdl-set-texture-blend-mode texture sdl:+sdl-blendmode-blend+)

        
        (check-sdl (sdl:sdl-update-texture texture
                                           (cffi:null-pointer)
                                           (static-vectors:static-vector-pointer (pngload:data png))
                                           (* 4 (pngload:width png)))))

      (sdl:sdl-show-window window))
    
    (make-instance 'console :window window
                            :renderer renderer
                            :rows rows :cols cols
                            :tileset tileset
                            :texture texture
                            :event (cffi:foreign-alloc 'sdl:sdl-event))))



(defun close-console (console)
  (decf *sdl-count*)
  (when (zerop *sdl-count*) (sdl:sdl-quit))
  (macrolet ((check-unset (slot-name action)
               `(when (and (slot-boundp console ',slot-name) (slot-value console ',slot-name))
                  (,action (slot-value console ',slot-name)))))
    (check-unset tileset-texture sdl:sdl-destroy-texture)
    (check-unset renderer sdl:sdl-destroy-renderer)
    (check-unset window sdl:sdl-destroy-window)
    (check-unset event cffi:foreign-free)))




(defmacro with-console ((name &key title rows cols tileset) &body body)
  (check-type name symbol)
  `(let* ((,name (open-console ,title ,rows ,cols ,tileset))
          (*default-console* ,name))
     (unwind-protect (progn ,@body)
       (close-console ,name))))



(defun background-color (&optional color (console *default-console*))
  (cffi:with-foreign-objects ((r :uint8) (g :uint8) (b :uint8) (a :uint8))
    (sdl:sdl-get-render-draw-color (renderer console) r g b a)
    (let ((color (or color (make-array 3 :element-type '(unsigned-byte 8) :initial-element 0))))
      (setf (aref color 0) r
            (aref color 1) g
            (aref color 2) b)
      color)))


(defun (setf background-color) (color &optional (console *default-console*))
  (sdl:sdl-set-render-draw-color (renderer console) (aref color 0) (aref color 1) (aref color 2) 255)
  color)



(defun clear (&optional (console *default-console*))
  (check-sdl (sdl:sdl-render-clear (renderer console)))
  (values))



(defun finish-drawing (&optional (console *default-console*))
  (check-sdl (sdl:sdl-render-present (renderer console)))
  (values))





;;; events

(defconstant left-button    sdl:+sdl-button-left+)
(defconstant middle-button  sdl:+sdl-button-middle+)
(defconstant right-button   sdl:+sdl-button-right+)
(defconstant button4        sdl:+sdl-button-x1+)
(defconstant button5        sdl:+sdl-button-x2+)


(defconstant a-key sdl:+sdl-scancode-a+)
(defconstant b-key sdl:+sdl-scancode-b+)
(defconstant c-key sdl:+sdl-scancode-c+)
(defconstant d-key sdl:+sdl-scancode-d+)
(defconstant e-key sdl:+sdl-scancode-e+)
(defconstant f-key sdl:+sdl-scancode-f+)
(defconstant g-key sdl:+sdl-scancode-g+)
(defconstant h-key sdl:+sdl-scancode-h+)
(defconstant i-key sdl:+sdl-scancode-i+)
(defconstant j-key sdl:+sdl-scancode-j+)
(defconstant k-key sdl:+sdl-scancode-k+)
(defconstant l-key sdl:+sdl-scancode-l+)
(defconstant m-key sdl:+sdl-scancode-m+)
(defconstant n-key sdl:+sdl-scancode-n+)
(defconstant o-key sdl:+sdl-scancode-o+)
(defconstant p-key sdl:+sdl-scancode-p+)
(defconstant q-key sdl:+sdl-scancode-q+)
(defconstant r-key sdl:+sdl-scancode-r+)
(defconstant s-key sdl:+sdl-scancode-s+)
(defconstant t-key sdl:+sdl-scancode-t+)
(defconstant u-key sdl:+sdl-scancode-u+)
(defconstant v-key sdl:+sdl-scancode-v+)
(defconstant w-key sdl:+sdl-scancode-w+)
(defconstant x-key sdl:+sdl-scancode-x+)
(defconstant y-key sdl:+sdl-scancode-y+)
(defconstant z-key sdl:+sdl-scancode-z+)

(defconstant 1-key sdl:+sdl-scancode-1+)
(defconstant 2-key sdl:+sdl-scancode-2+)
(defconstant 3-key sdl:+sdl-scancode-3+)
(defconstant 4-key sdl:+sdl-scancode-4+)
(defconstant 5-key sdl:+sdl-scancode-5+)
(defconstant 6-key sdl:+sdl-scancode-6+)
(defconstant 7-key sdl:+sdl-scancode-7+)
(defconstant 8-key sdl:+sdl-scancode-8+)
(defconstant 9-key sdl:+sdl-scancode-9+)
(defconstant 0-key sdl:+sdl-scancode-0+)

(defconstant return-key sdl:+sdl-scancode-return+)
(defconstant escape-key sdl:+sdl-scancode-escape+)
(defconstant backspace-key sdl:+sdl-scancode-backspace+)
(defconstant tab-key sdl:+sdl-scancode-tab+)
(defconstant space-key sdl:+sdl-scancode-space+)

(defconstant minus-key sdl:+sdl-scancode-minus+)
(defconstant equals-key sdl:+sdl-scancode-equals+)
(defconstant leftbracket-key sdl:+sdl-scancode-leftbracket+)
(defconstant rightbracket-key sdl:+sdl-scancode-rightbracket+)
(defconstant backslash-key sdl:+sdl-scancode-backslash+)

(defconstant hash-international-key sdl:+sdl-scancode-nonushash+)
(defconstant semicolon-key sdl:+sdl-scancode-semicolon+)
(defconstant apostrophe-key sdl:+sdl-scancode-apostrophe+)
(defconstant grave-key sdl:+sdl-scancode-grave+)
(defconstant comma-key sdl:+sdl-scancode-comma+)
(defconstant dot-key sdl:+sdl-scancode-period+)
(defconstant slash-key sdl:+sdl-scancode-slash+)

(defconstant capslock-key sdl:+sdl-scancode-capslock+)

(defconstant f1-key sdl:+sdl-scancode-f1+)
(defconstant f2-key sdl:+sdl-scancode-f2+)
(defconstant f3-key sdl:+sdl-scancode-f3+)
(defconstant f4-key sdl:+sdl-scancode-f4+)
(defconstant f5-key sdl:+sdl-scancode-f5+)
(defconstant f6-key sdl:+sdl-scancode-f6+)
(defconstant f7-key sdl:+sdl-scancode-f7+)
(defconstant f8-key sdl:+sdl-scancode-f8+)
(defconstant f9-key sdl:+sdl-scancode-f9+)
(defconstant f10-key sdl:+sdl-scancode-f10+)
(defconstant f11-key sdl:+sdl-scancode-f11+)
(defconstant f12-key sdl:+sdl-scancode-f12+)

(defconstant insert-key sdl:+sdl-scancode-insert+)
(defconstant home-key sdl:+sdl-scancode-home+)
(defconstant pageup-key sdl:+sdl-scancode-pageup+)
(defconstant delete-key sdl:+sdl-scancode-delete+)
(defconstant end-key sdl:+sdl-scancode-end+)
(defconstant pagedown-key sdl:+sdl-scancode-pagedown+)
(defconstant right-key sdl:+sdl-scancode-right+)
(defconstant left-key sdl:+sdl-scancode-left+)
(defconstant down-key sdl:+sdl-scancode-down+)
(defconstant up-key sdl:+sdl-scancode-up+)

(defconstant kp-divide-key sdl:+sdl-scancode-kp-divide+)
(defconstant kp-multiply-key sdl:+sdl-scancode-kp-multiply+)
(defconstant kp-minus-key sdl:+sdl-scancode-kp-minus+)
(defconstant kp-plus-key sdl:+sdl-scancode-kp-plus+)
(defconstant kp-enter-key sdl:+sdl-scancode-kp-enter+)
(defconstant kp-1-key sdl:+sdl-scancode-kp-1+)
(defconstant kp-2-key sdl:+sdl-scancode-kp-2+)
(defconstant kp-3-key sdl:+sdl-scancode-kp-3+)
(defconstant kp-4-key sdl:+sdl-scancode-kp-4+)
(defconstant kp-5-key sdl:+sdl-scancode-kp-5+)
(defconstant kp-6-key sdl:+sdl-scancode-kp-6+)
(defconstant kp-7-key sdl:+sdl-scancode-kp-7+)
(defconstant kp-8-key sdl:+sdl-scancode-kp-8+)
(defconstant kp-9-key sdl:+sdl-scancode-kp-9+)
(defconstant kp-0-key sdl:+sdl-scancode-kp-0+)
(defconstant kp-dot-key sdl:+sdl-scancode-kp-period+)

(defconstant backslash-international-key sdl:+sdl-scancode-nonusbackslash+)

(defconstant kp-comma-key sdl:+sdl-scancode-kp-comma+)

(defconstant lctrl-key sdl:+sdl-scancode-lctrl+)
(defconstant lshifht-key sdl:+sdl-scancode-lshift+)
(defconstant lalt-key sdl:+sdl-scancode-lalt+)
(defconstant lgui-key sdl:+sdl-scancode-lgui+)
(defconstant rctrl-key sdl:+sdl-scancode-rctrl+)
(defconstant rshift-key sdl:+sdl-scancode-rshift+)
(defconstant ralt-key sdl:+sdl-scancode-ralt+)
(defconstant rgui-key sdl:+sdl-scancode-rgui+)



(defmacro do-events (console &body clauses)
  (let ((event (gensym "EVENT"))
        (code (gensym "KEYCODE"))
        keydown keyup mousedown mouseup)
    (loop :for ((kind . specifiers) . actions) :in clauses :do
      (cond ((string= "press"   (string-downcase kind))
             (when (cdr specifiers)
               (error "Malformed PRESS clause: ~w~%Should only be 1 argument" specifiers))
             (push (cons (car specifiers) actions) keydown))
            
            ((string= "release" (string-downcase kind))
             (when (cdr specifiers)
               (error "Malformed RELEASE clause: ~w~%Should only be 1 argument" specifiers))
             (push (cons (car specifiers) actions) keyup))
            
            ((string= "click"   (string-downcase kind))
             (unless (= 3 (length specifiers))
               (error "Malformed CLICK clause: ~w~%Should be 3 arguments" specifiers))
             (push (cons specifiers actions) mousedown))
            
            ((string= "unclick" (string-downcase kind))
             (unless (= 3 (length specifiers))
               (error "Malformed UNCLICK clause: ~w~%Should be 3 arguments" specifiers))
             (push (cons specifiers actions) mouseup))

            (t (warn "Unknown event type: ~a" kind))))


    ;; This section is somewhat hairy, but i can't come up with a cleaner way to do this atm.
    ;; Note that each clause needs to be spliced/wrapped in an extra pair of parens, so that
    ;; no spurious empty lists remain in the case decleration causing BAD-FORM errors
    `(let ((,event (event ,console)))
       (loop :named magic :while (plusp (sdl:sdl-poll-event ,event)) :do
         (case (cffi:foreign-slot-value ,event 'sdl:sdl-event 'sdl:type)
           
           ,@(when keydown
               `((#.sdl:+sdl-keydown+
                  (let* ((,code (cffi:foreign-slot-value ,event 'sdl:sdl-keyboard-event 'sdl:keysym))
                         (,code (cffi:foreign-slot-value ,code 'sdl:sdl-keysym 'sdl:scancode)))
                    (cond ,@(loop :for (which . actions) :in (nreverse keydown) :collect `((eql ,code ,which) ,@actions)))))))

           ,@(when keyup
               `((#.sdl:+sdl-keyup+
                  (let* ((,code (cffi:foreign-slot-value ,event 'sdl:sdl-keyboard-event 'sdl:keysym))
                         (,code (cffi:foreign-slot-value ,code 'sdl:sdl-keysym 'sdl:scancode)))
                    (cond ,@(loop :for (which . actions) :in (nreverse keyup) :collect `((eql ,code ,which) ,@actions)))))))

           ,@(when mousedown
               `((#.sdl:+sdl-mousebuttondown+
                  (let ((,code (cffi:foreign-slot-value ,event 'sdl:sdl-mouse-button-event 'sdl:button)))
                    (cond ,@(loop :for ((which x y) . actions) :in (nreverse mousedown)
                                  :collect `((eql ,code ,which)
                                             (let ((,x (cffi:foreign-slot-value ,event 'sdl:sdl-mouse-button-event 'sdl:x))
                                                   (,y (cffi:foreign-slot-value ,event 'sdl:sdl-mouse-button-event 'sdl:y)))
                                               ,@actions))))))))

           ,@(when mouseup
               `((#.sdl:+sdl-mousebuttonup+
                  (let ((,code (cffi:foreign-slot-value ,event 'sdl:sdl-mouse-button-event 'sdl:button)))
                    (cond ,@(loop :for ((which x y) . actions) :in (nreverse mouseup)
                                  :collect `((eql ,code ,which)
                                             (let ((,x (cffi:foreign-slot-value ,event 'sdl:sdl-mouse-button-event 'sdl:x))
                                                   (,y (cffi:foreign-slot-value ,event 'sdl:sdl-mouse-button-event 'sdl:y)))
                                               ,@actions)))))))))))))
