;;;; package.lisp

(defpackage #:tomes
  (:use #:cl)
  (:local-nicknames (#:sdl #:raw-bindings-sdl2)
                    (#:a #:alexandria-2))
  (:export #:show
           #:show-colored
           #:with-color
           #:foreground-color
           #:open-console
           #:close-console
           #:with-console
           #:cols
           #:rows
           #:background-color
           #:clear
           #:finish-drawing
           #:do-events
           #:name
           #:make-encoding
           #:define-encoding
           #:encoding
           #:open-tileset
           #:find-glyph-id
           #:*cp437*
           #:*cp437-characters*

           #:F10-KEY #:W-KEY #:I-KEY #:7-KEY #:0-KEY #:E-KEY
           #:DELETE-KEY #:KP-COMMA-KEY #:LGUI-KEY #:K-KEY
           #:KP-4-KEY #:BUTTON4 #:KP-8-KEY #:RIGHTBRACKET-KEY
           #:LEFT-BUTTON #:F5-KEY #:O-KEY #:Q-KEY #:J-KEY
           #:KP-PLUS-KEY #:INSERT-KEY #:F2-KEY #:RIGHT-KEY
           #:KP-5-KEY #:KP-1-KEY #:A-KEY #:RSHIFT-KEY
           #:SEMICOLON-KEY #:M-KEY #:F4-KEY #:RETURN-KEY #:DOT-KEY
           #:KP-ENTER-KEY #:4-KEY #:U-KEY #:F-KEY #:RALT-KEY
           #:F1-KEY #:APOSTROPHE-KEY #:COMMA-KEY #:BACKSLASH-KEY
           #:UP-KEY #:END-KEY #:MIDDLE-BUTTON #:PAGEDOWN-KEY
           #:F11-KEY #:BACKSLASH-INTERNATIONAL-KEY #:H-KEY #:F7-KEY
           #:KP-MULTIPLY-KEY #:BUTTON5 #:LSHIFHT-KEY #:B-KEY
           #:3-KEY #:LALT-KEY #:P-KEY #:SLASH-KEY #:Y-KEY
           #:KP-6-KEY #:C-KEY #:HASH-INTERNATIONAL-KEY #:F12-KEY
           #:RCTRL-KEY #:KP-DIVIDE-KEY #:BACKSPACE-KEY #:HOME-KEY
           #:KP-3-KEY #:KP-7-KEY #:CAPSLOCK-KEY #:KP-MINUS-KEY
           #:L-KEY #:F3-KEY #:KP-9-KEY #:RGUI-KEY #:TAB-KEY
           #:GRAVE-KEY #:F9-KEY #:LCTRL-KEY #:R-KEY #:S-KEY
           #:G-KEY #:N-KEY #:MINUS-KEY #:KP-DOT-KEY #:2-KEY
           #:KP-0-KEY #:5-KEY #:6-KEY #:ESCAPE-KEY #:PAGEUP-KEY
           #:LEFT-KEY #:DOWN-KEY #:RIGHT-BUTTON #:Z-KEY #:V-KEY
           #:KP-2-KEY #:F8-KEY #:8-KEY #:D-KEY #:X-KEY
           #:LEFTBRACKET-KEY #:SPACE-KEY #:T-KEY #:F6-KEY #:1-KEY
           #:9-KEY #:EQUALS-KEY))
