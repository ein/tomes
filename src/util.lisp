
(in-package #:tomes)


(defmacro with-sdl-rects ((&rest bindings) &body body)
  (assert (loop :for (name . nil) :in bindings :always (and (not (keywordp name)) (symbolp name))))
  `(cffi:with-foreign-objects (,@(loop :for (name . nil) :in bindings :collect `(,name 'sdl:sdl-rect)))
     ,@(loop :for (name x y w h) :in bindings :collect `(setf (cffi:foreign-slot-value ,name 'sdl:sdl-rect 'sdl:x) ,x
                                                              (cffi:foreign-slot-value ,name 'sdl:sdl-rect 'sdl:y) ,y
                                                              (cffi:foreign-slot-value ,name 'sdl:sdl-rect 'sdl:w) ,w
                                                              (cffi:foreign-slot-value ,name 'sdl:sdl-rect 'sdl:h) ,h))
     (locally ,@body)))



(defun check-sdl (value)
  (when (or (and (cffi:pointerp value) (cffi:null-pointer-p value))
            (and (integerp value) (minusp value)))
    (error "Internal SDL error: ~a" (sdl:sdl-get-error)))
  value)
