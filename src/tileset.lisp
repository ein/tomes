

(in-package #:tomes)



(defclass encoding ()
  ((name :initarg :name :reader name)
   (transform-map :initarg :map :reader transform-map)))


(defun make-encoding (name mapping)
  (make-instance 'encoding :name name :map mapping))



(defmacro define-encoding (name &body mappings)
  (check-type name string)

  `(let* ((ht (make-hash-table :test 'equal))
          (characters nil))
    (loop :for c :in ',mappings :for i :from 0 :do
      (etypecase c
        (integer
         (setf (gethash c ht) i)
         (push (code-char c) characters))

        (cons
         (loop :for j :from i :repeat (car c) :do
           (setf (gethash (+ j (cadr c)) ht) j)
           (push (code-char (+ j (cadr c))) characters)
               :finally (incf i j)))))
     (defparameter ,(intern (format nil "*~a-CHARACTERS*" (string-upcase name))) (nreverse characters))
     (defparameter ,(intern (format nil "*~a*" (string-upcase name)))
       (make-encoding ,name ht))))



(defclass tileset ()
  ((path :initarg :path :reader path)
   (glyph-width :initarg :width :reader glyph-width)
   (glyph-height :initarg :height :reader glyph-height)
   (rows :initarg :rows :reader rows)
   (cols :initarg :cols :reader cols)
   (encoding :initarg :encoding :reader encoding)))



(defun open-tileset (filepath rows cols encoding)
  (unless (string= "png" (pathname-type filepath))
    (error "FILEPATH is not of type PNG"))
  (let* ((png (pngload:load-file filepath :decode nil))
         (width (pngload:width png))
         (height (pngload:height png)))
    (make-instance 'tileset :path filepath :rows rows :cols cols :encoding encoding
                            :width (floor width cols)
                            :height (floor height rows))))



(defun find-glyph-id (codepoint encoding)
  (cond ((gethash codepoint (transform-map encoding)))
        (t (warn "Encoding ~a has no glyph representing ~c" (name encoding) (code-char codepoint))
           0)))






;;; Standard encodings:

(define-encoding "cp437" 
  #x0000 #x263a #x263b #x2665 #x2666 #x2663 #x2660 #x2022 #x25d8 #x25cb #x25d9 #x2642 #x2640 #x266a #x266b #x263c
  #x25ba #x25c4 #x2195 #x203c #x00b6 #x00a7 #x25ac #x21a8 #x2191 #x2193 #x2192 #x2190 #x221f #x2194 #x25b2 #x25bc
  (95 0) #|| ascii plane, 95 symbols, no offset into the tileset ||#                                       #x2302
  #x00c7 #x00fc #x00e9 #x00e2 #x00e4 #x00e0 #x00e5 #x00e7 #x00ea #x00eb #x00e8 #x00ef #x00ee #x00ec #x00c4 #x00c5
  #x00c9 #x00e6 #x00c6 #x00f4 #x00f6 #x00f2 #x00fb #x00f9 #x00ff #x00d6 #x00dc #x00a2 #x00a3 #x00a5 #x20a7 #x0192
  #x00e1 #x00ed #x00f3 #x00fa #x00f1 #x00d1 #x00aa #x00ba #x00bf #x2310 #x00ac #x00bd #x00bc #x00a1 #x00ab #x00bb
  #x2591 #x2592 #x2593 #x2502 #x2524 #x2561 #x2562 #x2556 #x2555 #x2563 #x2551 #x2557 #x255d #x255c #x255b #x2510
  #x2514 #x2534 #x252c #x251c #x2500 #x253c #x255e #x255f #x255a #x2554 #x2569 #x2566 #x2560 #x2550 #x256c #x2567
  #x2568 #x2564 #x2564 #x2559 #x2558 #x2552 #x2553 #x256b #x256a #x2518 #x250c #x2588 #x2584 #x258c #x2590 #x2580
  #x03b1 #x00df #x0393 #x03c0 #x03a3 #x03c3 #x00b5 #x03c4 #x03a6 #x0398 #x03a9 #x03b4 #x221e #x03c6 #x03b5 #x2229
  #x2261 #x00b1 #x2265 #x2264 #x2320 #x2321 #x00f7 #x2248 #x00b0 #x2219 #x00b7 #x221a #x207f #x00b2 #x25a0 #x00a0)

