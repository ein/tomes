# Tomes

Tomes is a library for quickly developing graphical applications that
benefit from a character based grid for output, like roguelikes or CLI-applications, that would
traditionally run in a terminal and rely on ansi-escapes or curses-like libraries
to output structured graphical data.

Relying on a regular terminal however can cause problems with seemingly inoccous things
like color-support, as well as generally being less accessible to Windows users.
They are also usually geared more towards append-only printing, and not the
random access printing we would like to do.<br/>
Tomes on the other hand uses [SDL2](https://www.libsdl.org) to perform fast 
glyph-blitting to the screen, without the overhead or limitations that are usually associated with parsing ansi-escape codes.

Besides graphic output, Tomes also provides routines and macros to handle user-input and manage tilesets.<br/>
At the moment, tilesets based on the [CP437](https://en.wikipedia.org/wiki/Code_page_437)
encoding are supported out of the box, and custom encodings can be easily defined.<br/>
TrueType fonts are planned to be supported in the future.

### Installation

#### From Source
Tomes depends on [Raw-Bindings-SDL2](https://github.com/Zulu-Inuoe/raw-bindings-sdl2), 
which in turn depends on [SDL2 itself](https://libsdl.org/download-2.0.php).
Install SDL2 in the manner appropriate for your system,
then clone raw-bindings-sdl2 and the Tomes repository to a place ASDF knows to look. 
This should allow quicklisp to load tomes and its remaining dependencies as usual: `(ql:quickload:tomes)`.

By default, The system loads without it's documentation component. Once Tomes has been quickloaded once to load
the dependencies, using `(asdf:load-system :tomes/docs)` loads the system with documentation for development.  
Running `(tomes/docs:make)` will additionally build a local html documentation file, containing a 
symbol reference and a short tutorial.
