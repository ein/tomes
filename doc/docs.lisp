
(defpackage #:tomes/docs
  (:use :cl)
  (:local-nicknames (#:a #:alexandria-2))
  (:export #:make))

(in-package #:tomes/docs)


(defmacro defdocfun (sym string)
  (cond ((and (consp sym) (eq (car sym) 'setf))
         `(setf (documentation '(setf ,(find-symbol (string (cadr sym)) :tomes)) 'function) ,string))
        ((symbolp sym)
         `(setf (documentation ',(find-symbol (string sym) :tomes) 'function) ,string))
        (t (error "Can't set documentation for ~a" sym))))


(defmacro defdocvar (sym string)
  `(setf (documentation ',(find-symbol (string sym) :tomes) 'variable) ,string))


(defdocfun show
  "Draws OBJECT on CONSOLE at the position denoted by X and Y.
If OBJECT is a STRING, then each character of the string is printed in succession starting at X.
No provisions are made to check if all characters of the string are in-bounds of the console.

OBJECT: a CHARACTER, an INTEGER representing a unicode codepoint, or a STRING
X, Y: INTEGERs
CONSOLE: a CONSOLE

Signals:
- a CONTINUABLE-ERROR if OBJECT is not of a known type, or either X or Y are out of bounds for CONSOLE")


(defdocfun show-colored
  "Like SHOW, but accepts an additional color vector that is used to control the foreground drawing colour
of the console. The color-channels should be integers between 0..255 and are silently clamped.

OBJECT: a CHARACTER, an INTEGER representing a unicode codepoint, or a STRING
X, y: INTEGERs
COLOR: a VECTOR of 3 (UNSIGNED-BYTE 8)s
CONSOLE: a CONSOLE

Signals:
- a CONTINUABLE-ERROR if OBJECT is not of a known type, or either X or Y are out of bounds for CONSOLE")


(defdocfun with-color
  "Sets COLOR to be the foreground drawing color for all SHOW operations during its dynamic extent.
The color-channels should be integers between 0..255 and are silently clamped.
SHOW-COLORED still overrides the color value of WITH-COLOR.

CONSOLE: a CONSOLE
COLOR: a VECTOR of 3 (UNSIGNED-BYTE 8)s")



(defdocfun foreground-color
  "Retrieves the default foreground-drawing color of the console as a vector representing the R G B channels.

CONSOLE: a CONSOLE
=> (VECTOR (UNSIGNED-BYTE 8) 3)")


(defdocfun (setf foreground-color)
  "Sets the default foreground-drawing color of the console to the supplied color.
The color-channels should be integers between 0..255 and are silently clamped.

COLOR: a VECTOR of 3 (UNSIGNED-BYTE 8)s
CONSOLE: a CONSOLE
=> COLOR")



(defdocfun open-console
  "Opens a CONSOLE, i.E. a window which is sized precisely to hold ROW*COLS many characters from the TILESET.

TITLE: a STRING, used as the window title for the underlying OS-window
ROWS, COLS: INTEGERs used to specify the size of the window
TILESET: a TILESET, used as the underlying base-tileset to size the console
=> a fresh CONSOLE")


(defdocfun close-console
      "Cleans up resources held by the console, and closes the accompanying OS-window.

CONSOLE: a CONSOLE")


(defdocfun with-console
      "Creates a fresh CONSOLE and binds it to NAME as well as *DEFAULT-CONSOLE*.
The CONSOLE object bound to NAME has dynamic-extent.

NAME: a SYMBOL
TITLE: a STRING, used as the window title
ROWS, COLS: INTEGERs describing the size of the console window
TILESET: a TILESET, used as the base-tileset for the console")


(defdocfun background-color
      "Returns the color used to fill in the background of CONSOLE.

CONSOLE: a CONSOLE
COLOR: NIL, or a VECTOR of at least 3 elements filled in with the color channels")


(defdocfun (setf background-color)
      "Sets the color used to fill in the background of CONSOLE.

CONSOLE: a CONSOLE
COLOR: a VECTOR of the shape #(R G B), representing each channel as an integer 0-255")


(defdocfun clear
  "Overpaints all content in the CONSOLE with the background-color.

CONSOLE: a CONSOLE")


(defdocfun finish-drawing
  "Commits all actions from draw-calls like SHOW or CLEAR and presents them to be actually
visible in the window.

CONSOLE: a CONSOLE")


(defdocfun do-events
  "Loops through all pending events of CONSOLE and tests it against the first element of the each of the CLAUSES.
A clause is of the form ((event-type . event-data) actions...), where event-data depends on the event-type.

For the event-types PRESS and RELEASE, the event-data is a form to be evaluated to a keysym, and compared to
the keysym of the event. If they match, the actions are executed in order and DO-EVENTS continues.

For the event-types CLICK and UNCLICK, the event-data is a form to be evaluted to a button-sym and two symbols.
if the first form matches the button-sym data in the event, then the symbols are bound to the X and Y coordinates
where the button click occured, and the actions are executed.

CONSOLE: a CONSOLE
CLAUSES: a list of the form ((EVENT-TYPE . EVENT-DATA) &rest ACTIONS), handled as described above

Signals:
- An ERROR if any of the clauses are malformed
- A WARNING if a clause contains an event-type that is not known.")


(defdocfun make-encoding
  "Creates a new tileset-encoding which maps the positions of glyphs in
the tileset to unicode-codepoints.

NAME: a STRING
MAPPINGS: a HASH-TABLE, mapping unicode-codepoints to GLYPH-IDs
=> an ENCODING")


(defdocfun define-encoding
  "Defines *NAME* to be a new tileset-encoding named NAME,
which maps the positions of glyphs in the tileset to CHARACTERS.
Additionally defines *NAME-CHARACTERS* to be a list of all available codepoints this encoding maps.

NAME: a STRING, symbolicated for the names of the bound variables as well as the NAME slot in the ENCODING
MAPPINGS: a list of either INTEGERs representing unicode-codepoints, or a list of the form (length offset),
          encoding the next LENGTH characters to be mapped to (- INDEX OFFSET)")


(defdocfun open-tileset
  "Creates a TILESET.

FILEPATH: a STRING or PATHNAME, pointing to the file to be used as a tileset
ROWS, COLS: INTEGERs describing the layout of the file and tileset
ENCODING: an ENCODING describing the glyph-order of the file and tileset
=> a TILESET

Signals:
- An ERROR if FILEPATH does not point to a PNG-File")


(defdocfun find-glyph-id
  "Attempts to find CODEPOINT in ENCODING and returns the corresponding GLYPH-ID. 
If ENCODING has no representation for CODEPOINT, GLYPH-ID will be 0.

CODEPOINT: an INTEGER representing a unicode code-point
ENCODING: an ENCODING
=> GLYPH-ID: an INTEGER representing the index into the texture of a TILESET

Signals:
- A WARNING if CODEPOINT has no representation in TILESET")



(defdocvar *cp437*
  "The classic IBM Code-Page 437 encoding, as used by e.g. Dwarf Fortress")

(defdocvar *cp437-characters*
  "A list of all glyphs available in a CP437 encoding")

(defdocvar *default-console*
  "The default console to write to. Established as a context by WITH-CONSOLE")




(defun format-for-html (stream string)
  (format stream "<blockquote>")
  (with-input-from-string (in string)
    (loop :for line := (read-line in nil) :while line :do
      (format stream "~a<br/>" line)))
  (format stream "</blockquote>"))


(defun make ()
  (with-open-file (out (asdf:system-relative-pathname :tomes "doc/tomes.html")
                       :direction :output :if-exists :supersede :if-does-not-exist :create)

    (format out "<!DOCTYPE html>~%")
    (format out "<html lang=\en\">~%")
    (format out "<head><meta charset=UTF-8>~%<title>Tomes Manual</title><link rel=\"stylesheet\" href=\"./style.css\"></head>~%")
    (format out "<body>~%")
    
    (with-open-file (in (asdf:system-relative-pathname :tomes "doc/overview.html"))
      (loop :for line := (read-line in nil) :while line :do
        (format out "~a~%" line)))

    (let ((tomes (find-package :tomes))
          vars funs macros)
      (do-external-symbols (sym tomes)
        (when (eq (symbol-package sym) tomes)
          (when (documentation sym 'variable)
            (push sym vars))
          
          (when (documentation sym 'function)
            (if (macro-function sym)
                (push sym macros)
                (push sym funs)))))

      (flet ((menufy-syms (syms)
               (format out "<menu>")
               (dolist (sym syms)
                 (format out "<li><a href=\"#~a\">~a</a></li>" (string-downcase sym) sym))
               (format out "</menu>")))
        (format out "<menu>")
        (format out "<li><a href=\"#variables\">Variables</a></li>")
        (menufy-syms vars)
        (format out "<li><a href=\"#functions\">Functions</a></li>")
        (menufy-syms funs)
        (format out "<li><a href=\"#macros\">Macros</a></li>")
        (menufy-syms macros)
        (format out "</menu>"))

      (format out "<h3 id=\"variables\">Variables</h3>")
      (dolist (sym vars)
        (format out "<p id=\"~a\"><strong>~a</strong><br/>" (string-downcase sym) sym)
        (format-for-html out (documentation sym 'variable))
        (format out "</p>~%"))

      (format out "<h3 id=\"functions\">Functions</h3>")
      (dolist (sym funs)
        (format out "<p id=\"~a\"><strong>~a</strong> " (string-downcase sym) sym)
        #+sbcl(format out "<i>~a</i><br/>" (sb-introspect:function-lambda-list sym))
        (format-for-html out (documentation sym 'function))
        (when (documentation `(setf ,sym) 'function)
          (format out "<strong>setf ~a</strong> " sym)
          #+sbcl(format out "<i>~a</i><br/>" (sb-introspect:function-lambda-list `(setf ,sym)))
          (format-for-html out (documentation `(setf ,sym) 'function)))
        (format out "<p><br/>"))

      (format out "<h3 id=\"macros\">Macros</h3>")
      (dolist (sym macros)
        (format out "<p id=\"~a\"><strong>~a</strong> " (string-downcase sym) sym)
        #+sbcl(format out "<i>~a</i><br/>" (sb-introspect:function-lambda-list sym))
        (format-for-html out (documentation sym 'function))
        (when (documentation `(setf ,sym) 'function)
          (format out "<strong>setf ~a</strong> " sym)
          #+sbcl(format out "<i>~a</i><br/>" (sb-introspect:function-lambda-list `(setf ,sym)))
          (format-for-html out (documentation `(setf ,sym) 'function)))
        (format out "<p><br/>")))

    (format out "</body>~%</html>")))
