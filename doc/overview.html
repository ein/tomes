

<h1>Tomes</h1>

<p>Contents</p>
<ol>
  <li><a href="#overview">Overview</a></li>
  <li><a href="#example">Example Application Walkthrough</a></li>
  <li><a href="#api">API Reference</a></li>
</ol>



<h2 id="overview">Overview</h2>

<p>
  <i>Tomes</i> is a library for quickly developing graphical applications that
  benefit from a character based grid for output, like roguelikes or CLI-applications, that would
  traditionally run in a terminal and rely on ansi-escapes or curses-like libraries
  to output structured graphical data.
</p>

<p>
  Relying on a regular terminal however can cause problems with seemingly inoccous things
  like color-support, as well as generally being less accessible to Windows users.
  They are also usually geared more towards append-only printing, and not the
  random access printing we would like to do.<br/>
  <i>Tomes</i> on the other hand uses <a href="https://www.libsdl.org">SDL2</a> to perform fast 
  glyph-blitting to the screen, without the overhead or limitations that are usually associated with parsing ansi-escape codes.
</p>

<p>
  Besides graphic output, <i>Tomes</i> also provides routines and macros to handle user-input and manage tilesets.<br/>
  At the moment, tilesets based on the <a href="https://en.wikipedia.org/wiki/Code_page_437">CP437</a>
  encoding are supported out of the box, and custom encodings can be easily defined.<br/>
  TrueType fonts are planned to be supported in the future.
</p>


<h2 id="example">Example Application Walkthrough</h2>
<p>
  <i>Tomes</i> is fairly easy to set up and work with, but it can sometimes be awkward
  to relate which lisp character corresponds to which glyph in the tileset you have chosen.<br/>
  Luckily, we can use <i>Tomes</i> itself to help by writing a short utility program that
  prints each character an encoding knows about and it's corresponding glyph.<br/>
  The code for this application is shown below, followed by a short walkthrough of
  <i>Tomes</i> specific functionality that makes it work.
</p>

<pre>
(defpackage :simple-example
  (:use cl))

(in-package :simple-example)

(defun list-available-characters ()
  (tomes:with-console (con :title "Character listing" :rows 60 :cols 80
                           ;; We'll be using the excellent "Buddy" tileset made by Buddy
                           :tileset (tomes:open-tileset "Buddy.png" 16 16 tomes:*cp437*)) 
    (setf (tomes:background-color con) #(0 0 0))
    (tomes:clear con)

    (loop :for i :from 0 :for char :in tomes:*cp437-characters* :do
      (tomes:show (format nil "~a : ~:*~w" char) 0 i)
      (when (and (not (zerop i)) (zerop (mod i 59)))
        (tomes:finish-drawing)
        (loop
          (tomes:do-events con
            ((release tomes:space-key) (return))))
        (setf i -1)
        (tomes:clear))))))
</pre>

<p>
  When the function <code>list-available-characters</code> is called, a window should appear looking similar to:<br/>
  <img src="./list-characters-compressed.png" alt="A window showing a series of lines of characters, followed by a colon and a lisp character form"/>
  <br/>
  Pressing the spacebar should page through the available characters, until the last
  page is reached where the application terminates.
</p>

<p>
  <code>(tomes:with-console (con :title "Character listing" :rows 60 :cols 80</code><br/>
  A <i>Tomes</i> program usually starts with <a href="#with-console"><code>WITH-CONSOLE</code></a> to set up
  a console-context to draw to and receive events from. Here, we name our console <code>CON</code>, and request
  it to be 60 rows high and 80 columns wide. The exact size of the console will be calculated from the
  primary tileset passed to the macro on the next line:<br/>
  <code>:tileset (tomes:open-tileset "Buddy.png" 16 16 tomes:*cp437*))</code><br/>
  This constructs a new tileset description from the path to the PNG file to be used for the glyphs,
  how many rows and columns of glyphs are in the texture, and with which encoding the glyphs are laid out.
</p>

<p>
  Next up we set up the background color that console should be cleared to, and clear the screen to remove
  any spurious old pixel data that may have been left in the window after creating it.<br/>
  <code>(setf (tomes:background-color con) #(0 0 0))</code> simply sets the background color to be all black.<br/>
  Colors in <i>Tomes</i> should be specified as bytes, that is integers from 0..255, in RGB order, and are usually passed
  around in vectors.
</p>

<p>
  Moving on, we enter a loop that loops through the contents of
  <a href="#*cp437-characters*"><code>tomes:*cp437-characters*</code>.<br/></a>
  <i>Tomes</i> automatically generates these kind of variables (that is, of the form <code>*encoding-name-characters*</code>)
  to hold a list of all the characters an encoding specified glyphs for. This makes it easy to inspect which characters
  an encoding supports, and in which order they occur. 
</p>

<p>
  Now we are ready to finally output something to the screen.<br/>
  <code>(tomes:show (format nil "~a : ~:*~w" char) 0 i)</code> uses the character we are currently looking at
  and formats it into a string that first displays the character in it's display form, and then how it would be
  represented in lisp source text.<br/>
  The resulting string is then passed on to <a href="#show"><code>show</code></a> which loops throug it's characters and prints them
  to our console. Each character is positioned at the left most edge of the window, I lines down.<br/>
  One oddity here is that we have omitted to specify <code>con</code> as our target to show to. In fact,
  <code>with-console</code> conviniently binds the dynamic variable
  <a href="#*default-console*"><code>*default-terminal*</code></a> to
  the consule it constructs. Many of the <i>Tomes</i> function take a console as an <i>optional</i> argument,
  and default to <code>*default-console*</code> if no explicit console is given. This makes the code much
  easier to read and write if only one console is in scope.
</p>

<p>
  After having drawn our string to the console, we check if we have already filled up the whole console with
  lines. Attempting to write characters out of bounds is generally considered an error, so we must guard
  against accidentally showing too many lines at once.<br/>
  If we have produces enough lines, we call <code>(tomes:finish-drawing)</code> to actually update
  the window contents. <i>Tomes</i> will not actually update the window until this function is called,
  and all buffered state accumulated from the various calls to <code>clear</code> and <code>show</code>
  will be committed to the window at once.
</p>

<p>
  Moreover, we also wish to pause the application here so the user can review the character information we have
  printed to the console.<br/>
  To do so we enter an infinite loop and then use the <a href="#do-events"><code>do-events</code></a> macro
  to poll for events coming from the user or the system.<br/>
  The clauses for <code>do-events</code> vary depending on the event type. Here we only check if the space key
  has been pressed, and if so <code>return</code> to break out of the infinite loop, once more <code>clear</code>
  the console (this time, omitting the <code>con</code> parameter) and finally
  advance further through the encoding-glyph-list and print the next page of glyphs.
</p>


<h2 id="api">API Reference</h2>


