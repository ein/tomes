

(defsystem #:tomes/raw-bindings-sdl2
  :name "raw-bindings-sdl2"
  :description "Bindings and minor utilities for SDL2"
  :author "Wilfredo Velázquez-Rodríguez <zulu.inuoe@gmail.com>"
  :license "CC0 <http://creativecommons.org/publicdomain/zero/1.0/>"
  :serial t
  :components
  ((:module "raw-bindings-sdl2"
             :serial t
             :components
             ((:file "package")
              (:file "macros")
              (:file "library")

              (:file "sdl-temp-string")
              (:module "ffi"
                       :components
                       ((:file "stdinc")

                        (:file "blendmode")
                        (:file "clipboard")
                        (:file "cpuinfo")
                        (:file "error")
                        (:file "filesystem")
                        (:file "hints")
                        (:file "joystick")
                        (:file "loadso")
                        (:file "log")
                        (:file "main")
                        (:file "pixels")
                        (:file "power")
                        (:file "rect")
                        (:file "rwops")
                        (:file "scancode")
                        (:file "timer")
                        (:file "touch")
                        (:file "version")

                        (:file "audio")
                        (:file "gamecontroller")
                        (:file "haptic")
                        (:file "keycode")
                        (:file "surface")

                        (:file "gesture")
                        (:file "video")

                        (:file "messagebox")
                        (:file "mouse")
                        (:file "keyboard")
                        (:file "render")
                        (:file "shape")
                        (:file "syswm")

                        (:file "events")

                        (:file "quit")

                        (:file "sdl"))))))
  :depends-on
  (#:cffi
   #:defpackage-plus))


(asdf:defsystem #:tomes
    :description "A small library for tilegrid based graphical UI."
    :author "Ein Kathage <siboru@googlemail.com>"
    :license  "lGPLv2"
    :depends-on (:tomes/raw-bindings-sdl2 :cffi :pngload :alexandria)
    :serial t
    :components ((:file "src/package")
                 (:file "src/util")
                 (:file "src/globals")
                 (:file "src/console")
                 (:file "src/tileset")
                 (:file "src/tomes")))



(asdf:defsystem #:tomes/docs
  :description "Documentation for the TOMES library"
  :author "Ein Kathage <siboru@googlemail.com>"
  :license "lGPLv2"
  :depends-on (:tomes)
  :serial t
  :components ((:file "doc/docs")))


