

(defpackage :tomes.messages.simple
  (:use :cl))

(in-package :tomes.messages.simple)


(eval-when (:compile-toplevel :load-toplevel :execute)
  (asdf:load-system :tomes)
  (load (asdf:system-relative-pathname :tomes "src/messages.lisp"))
  (ql:quickload :swank))



(defparameter *tileset-path* (asdf:system-relative-pathname :tomes "resources/Buddy.png"))


(defclass player ()
  ((x :initform 0)
   (y :initform 0)
   (health :initform 100)))



(defclass skeleton ()
  ((x :initform 0)
   (y :initform 0)
   (health :initform 10)
   (colour :initform #(255 255 255))))



(defparameter *arena*
  (make-array '(10 11) :element-type 'base-char
              :initial-contents '("###########"
                                  "#   #s#   #"
                                  "#   # #   #"
                                  "#   # #   #"
                                  "#         #"
                                  "#         #"
                                  "#         #"
                                  "#    @    #"
                                  "#         #"
                                  "###########")))



(defclass attack ()
  ((damage :initarg :damage)))


(defclass death ()
  ((target :initarg :target)
   (game-ending? :initarg :game-ending :initform nil)))


(defmethod tomes::send-message ((attack attack) (skeleton skeleton))
  (decf (slot-value skeleton 'health) (slot-value attack 'damage))
  (when (<= (slot-value skeleton 'health) 0)
    (tomes::post-message (make-instance 'death :target skeleton)))
  (setf (slot-value skeleton 'colour) #(255 0 0)))


(defmethod tomes::send-message ((attack attack) (player player))
  (decf (slot-value player 'health) (slot-value attack 'damage))
  (when (<= (slot-value player 'health) 0)
    (tomes::post-message (make-instance 'death :target player :game-ending t))))




(defun move-player (player map)
  (labels ((mover (msg)
             (let ((key (tomes::key-sym msg))
                   (px (slot-value player 'x))
                   (py (slot-value player 'y)))
               ;; Get new position
               (case key
                 (#.tomes:w-key (decf py))
                 (#.tomes:s-key (incf py))
                 (#.tomes:a-key (decf px))
                 (#.tomes:d-key (incf px)))
               ;; Check if we can move there:
               (unless (and (<= 0 px 10) (<= 0 py 9))
                 (return-from mover))
               (let ((cell (aref map py px)))
                 (cond ((null cell)
                        ;; empty map slot
                        (setf (aref map (slot-value player 'y) (slot-value player 'x)) nil)
                        ;; move player
                        (setf (slot-value player 'x) px)
                        (setf (slot-value player 'y) py)
                        ;; enter player into map
                        (setf (aref map py px) player))
                       ((typep cell 'skeleton)
                        (tomes::send-message (make-instance 'attack :damage (1+ (random 3))) cell))
                       ((eq :wall cell)
                        ;; can't move, do nothing
                        ))))))
    #'mover))




(defun main ()
  (let ((tileset (tomes:open-tileset *tileset-path* 16 16 tomes:*cp437*))
        (running t)
        (player (make-instance 'player))
        (skellyboi (make-instance 'skeleton))
        map)
    (tomes:with-console (con :title "Simple Example" :rows 60 :cols 80 :tileset tileset)
      (tomes::initialize-default-messages)
      (setf (tomes:background-color con) #(0 0 0))
      (tomes:clear con)
      (tomes:finish-drawing con)

      ;; register our messages
      (tomes::register-message (find-class 'death))
      (tomes::register-message (find-class 'attack))
             
      (tomes::subscribe (event tomes::key-up)
        (let ((key (tomes::key-sym event)))
          (case key
            (#.tomes:escape-key (setf running nil)))))
      
      ;; Initialize map from *arena*
      (setf map (make-array '(10 11) :initial-element nil))
      (loop :for y :below 10 :do
        (loop :for x :below 11 :do
          (setf (aref map y x) (case (aref *arena* y x)
                                 (#\#
                                  :wall)
                                 (#\s ;; move the skellyboi
                                  (setf (slot-value skellyboi 'x) x
                                        (slot-value skellyboi 'y) y)
                                  skellyboi)
                                 (#\@ ;; move the player
                                  (setf (slot-value player 'x) x
                                        (slot-value player 'y) y))))))

      ;; register player handling
      (tomes::subscribe-to (find-class 'tomes::key-down) (move-player player map))

      ;; register death handling
      (tomes::subscribe (msg death)
        (let ((target (slot-value msg 'target))
              (game-ending? (slot-value msg 'game-ending?)))
          (when game-ending?
            (setf running nil))
          ;; remove entity from the map
          (setf (aref map (slot-value target 'y) (slot-value target 'x)) nil)))
  
      ;; Draw Frame:
      (let ((last-frame-time 1.0)
            (ms internal-time-units-per-second))
        (loop :while running :do
          (let ((start (get-internal-real-time)))
            (tomes:clear)
            (setf (slot-value skellyboi 'colour) #(255 255 255))
            (tomes::post-system-messages con)
            (loop :for y :below 10 :do
              (loop :for x :below 11 :do
                (let ((cell (aref map y x))
                      (sprite #\ )
                      (colour #(255 255 255)))
                  (cond ((eq :wall cell)
                         (setf sprite #\#))
                        ((typep cell 'player)
                         (setf sprite #\@))
                        ((typep cell 'skeleton)
                         (setf sprite #\S)
                         (setf colour (slot-value cell 'colour))))
                  (tomes:show-colored sprite x y colour))))
            (tomes:show (format nil "~9,6,,,'0f seconds (~a FPS)"
                                last-frame-time (/ 1.0 (max last-frame-time 0.000001)))
                        0 0)
            (setf last-frame-time (/ (float (- (get-internal-real-time) start)) ms))
            (tomes:finish-drawing)))))))



;; Workaround for a bug with windows. Open a terminal (or CMD etc),
;; then run: sbcl --load messages.lisp
;; then open emacs, M-x slime-connect RET localhost RET 5555 RET to connect
;; afterwards, you should be able to develop as usual and run MAIN, without windows
;; peeing its pants because rendering happens in a non-main thread
(swank:create-server :port 5555 :dont-close t)
(loop (sleep 1))
