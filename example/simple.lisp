
(defpackage :simple-example
  (:use cl))

(in-package :simple-example)


(defvar w tomes:w-key)
(defvar a tomes:a-key)
(defvar s tomes:s-key)
(defvar d tomes:d-key)

(defvar escape tomes:escape-key)
(defvar spacekey tomes:space-key)

(defvar lmb tomes:left-button)


(defun main ()
  (let ((tileset (tomes:open-tileset (asdf:system-relative-pathname :tomes "resources/Buddy.png") 16 16 tomes:*cp437*)))
    (tomes:with-console (con :title "Simple Example" :rows 60 :cols 80 :tileset tileset)
      (setf (tomes:background-color con) #(0 0 0))
      (tomes:clear con)
      (tomes:finish-drawing con)
      (let ((playerx (floor (tomes:cols con) 2))
            (playery (floor (tomes:rows con) 2))
            (bangx 0)
            (bangy 0))
        (loop :with running := t :while running :do
          (tomes:clear)
          (tomes:show #\@ playerx playery) ;; don't need to specify console here due to the WITH-CONSOLE above
          (tomes:show #\! bangx bangy)
          (tomes:finish-drawing)
          (tomes:do-events con
            ((release escape) (setf running nil))
            ((press w) (decf playery))
            ((press s) (incf playery))
            ((press a) (decf playerx))
            ((press d) (incf playerx))
            ((click lmb x y) (setf bangx (floor x 10)
                                   bangy (floor y 10)))))))))





(defun list-available-characters ()
  (let ((tileset (tomes:open-tileset (asdf:system-relative-pathname :tomes "resources/Buddy.png") 16 16 tomes:*cp437*)))
    (tomes:with-console (con :title "Character listing" :rows 60 :cols 80 :tileset tileset)
      (setf (tomes:background-color) #(0 0 0))
      (tomes:clear)
      
      (loop :for i :from 0 :for char :in tomes:*cp437-characters* :do
        (tomes:show (format nil "~a : ~:*~w" char) 0 i)
        (when (and (not (zerop i)) (zerop (mod i 59)))
          (tomes:finish-drawing)
          (loop
            (tomes:do-events con
              ((release spacekey) (return))))
          (setf i -1)
          (tomes:clear))))))



